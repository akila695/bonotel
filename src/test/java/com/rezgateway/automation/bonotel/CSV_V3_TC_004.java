package com.rezgateway.automation.bonotel;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.rezgateway.automation.JavaHttpHandler;
import com.rezgateway.automation.builder.request.AvailabilityRequestBuilder;
import com.rezgateway.automation.input.CsvReader;
import com.rezgateway.automation.pojo.AvailabilityRequest;
import com.rezgateway.automation.pojo.AvailabilityResponse;
import com.rezgateway.automation.pojo.DailyRates;
import com.rezgateway.automation.pojo.HttpResponse;
import com.rezgateway.automation.pojo.RateplansInfo;
import com.rezgateway.automation.pojo.Room;
import com.rezgateway.automation.reader.response.AvailabilityResponseReader;
import com.rezgateway.automation.reports.ExtentTestNGReportBuilderExt;
import com.rezgateway.automation.xmlout.utill.DataLoader;
import com.rezgateway.automation.xmlout.utill.ExcelDataSingleton;

public class CSV_V3_TC_004 extends ExtentTestNGReportBuilderExt {
	AvailabilityResponse AvailResponse = new AvailabilityResponse();
	AvailabilityRequest AvailRequest = new AvailabilityRequest();	
	DecimalFormat df = new DecimalFormat("#.00");
	
	@Parameters("TestUrl")
	@Test(priority = 0)
	public synchronized void availbilityTest(String TestUrl) throws Exception {
		ITestResult result = Reporter.getCurrentTestResult();
		String Scenario     = AvailRequest.getScenarioID();
		String HotelCode    = Arrays.toString(AvailRequest.getCode());
		String SearchString = AvailRequest.getCheckin()+"|"+AvailRequest.getCheckout()+" |"+AvailRequest.getNoOfRooms()+"R| "+AvailRequest.getUserName()+" |"+AvailRequest.getPassword();
		String testname = "Test Scenario:" + Scenario + " : Search By : " + AvailRequest.getSearchType() + " Code : " + HotelCode+ "Criteria : "+SearchString;
		
		if (getAvailabilityData() == null) {
			result.setAttribute("Actual", "Scenario id :" + Scenario);
			Assert.fail("Search criteria has not mentioned...");
		} else {
			getAvailabilityData();		
			
			result.setAttribute("TestName", testname);
			result.setAttribute("Expected", "Results Should be available");

			JavaHttpHandler handler = new JavaHttpHandler();
			HttpResponse Response = handler.sendPOST(TestUrl, "xml=" + new AvailabilityRequestBuilder().buildRequest("Resources/CsvSuite_AvailRequest_senarioID_" + AvailRequest.getScenarioID() + ".xml", AvailRequest));
			
			System.out.println("xml=" + new AvailabilityRequestBuilder().buildRequest("Resources/CsvSuite_AvailRequest_senarioID_" + AvailRequest.getScenarioID() + ".xml", AvailRequest));
			System.out.println(Response.getRESPONSE());
			
			if(Response.getRESPONSE_CODE() == 200) {
				AvailResponse = new AvailabilityResponseReader().getResponse(Response.getRESPONSE());
							
				if(AvailResponse.getHotelCount() > 0) {
					result.setAttribute("Actual", "Results Available, Hotel Count :" + AvailResponse.getHotelCount());
				}else {
					result.setAttribute("Actual", "Results not available Error Code :" + AvailResponse.getErrorCode() + " Error Desc :" + AvailResponse.getErrorDescription());
					Assert.fail("No Results Error Code :" + AvailResponse.getErrorCode());
				}
			}else {
				result.setAttribute("Actual", "No Response recieved Code :" + Response.getRESPONSE_CODE());
				Assert.fail("Invalid Response Code :" + Response.getRESPONSE_CODE() + " ,No Response received");
			}	
		}		
	}
	
	
	@Test(dependsOnMethods = "availbilityTest")
	public synchronized void testHotelCode() {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing hotel code");
		result.setAttribute("Expected", "Hotel code in availability request and response should be same");
		
		if (AvailResponse.getHotelList().containsKey(AvailRequest.getCode()[0])) {
			result.setAttribute("Actual", AvailResponse.getHotelList().entrySet().iterator().next().getKey());			
		} else {
			result.setAttribute("Actual", "Requested Hotel code (" + AvailRequest.getCode()[0] + ")  is not available ,Hotel Code is " + AvailResponse.getHotelList().entrySet().iterator().next().getKey());
			Assert.fail("Requested Hotel code is not available");
		}
	}	
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testHotelCode")
	public synchronized void testCsvDataAvailability(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing Availability of csv record");
		result.setAttribute("Expected", "Data should be available according to the search criteria");
		
		Map<String,String[]> mapData = readCsvData(filePath);
		if(mapData == null){
			result.setAttribute("Actual", "Issue in csv file." );
			Assert.fail("Issue in csv file.");
		}else if(mapData.size()>0){
			result.setAttribute("Actual", "Record count is  " + readCsvData(filePath).size());
		}else{
			result.setAttribute("Actual", "Record count is  " + readCsvData(filePath).size());
			Assert.fail("No records available in csv");
		}				
	}
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testCsvDataAvailability")
	public synchronized void testCsvHeaders(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing csv headers");
		result.setAttribute("Expected", "Headers should be in this order. StartDate | EndDate | Duration | PlaceCode | BuildingID | BuildingName | RoomTypeId | BoardBasisId | BoardBasisCode | BoardBasisFullName | Adults | Children | Daily Rates Before Promotion | Total Rate Before Promotion | Daily Rates After Promotion | Total Rate After Promotion | WasPrice | P1SellingPrice | P2SellingPrice | P3SellingPrice | CH1SellingPrice | CH2SellingPrice | CH3SellingPrice | TotalSupplementPrice | CompulsorySupplements | Promotion Code | Promotion ID | roomCode | bedTypeID");
		
		boolean withHeader = true;
		String expectedHeaders = "StartDate | EndDate | Duration | PlaceCode | BuildingID | BuildingName | RoomTypeId | BoardBasisId | BoardBasisCode | BoardBasisFullName | Adults | Children | Daily Rates Before Promotion | Total Rate Before Promotion | Daily Rates After Promotion | Total Rate After Promotion | WasPrice | P1SellingPrice | P2SellingPrice | P3SellingPrice | CH1SellingPrice | CH2SellingPrice | CH3SellingPrice | TotalSupplementPrice | CompulsorySupplements | Promotion Code | Promotion ID | roomCode | bedTypeID | ";
		String actualHeaders = "";		
					
		try {
			CsvReader reader = new CsvReader();	
			CSVParser parser = reader.csvRead(filePath, withHeader);
			parser.getHeaderMap();
			
			for (Map.Entry<String, Integer> entry : parser.getHeaderMap().entrySet()){
				actualHeaders += entry.getKey().trim() + " | ";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		String expectedSimple = expectedHeaders.toLowerCase();
		String actualSimple = actualHeaders.toLowerCase();
		
		if(expectedSimple.equals(actualSimple)){
			result.setAttribute("Actual", "Headers are correct.");
			System.out.println("Headers are correct. Expected headers : " + expectedSimple + "\tIn csv : " + actualSimple);
		}else{
			result.setAttribute("Actual", "Headers are incorrect." + "Expected headers : " + expectedSimple + "\tIn csv : " + actualSimple);
			Assert.fail("Headers are incorrect. Expected headers : " + expectedSimple + "\tIn csv : " + actualSimple);
		}
	}
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testCsvDataAvailability")
	public synchronized void testRoomCount(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing room count");
		result.setAttribute("Expected", "Room count in csv and response xml should be same");
		
		String xmlRoomCodesData = "";
		String csvRoomCodesData = "";
		
		Map<String,String[]> mapData = readCsvData(filePath);
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			csvRoomCodesData += entry.getKey() + ",";
		}
		
		int xmlRoomCount = 0;
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			String roomType = room.getRoomType();
			String landOnly = roomType.substring(Math.max(roomType.length() - 3, 0));
			if(!landOnly.equals("L/O")){
				xmlRoomCount++;
				String xmlRoomCode = room.getRoomCode();
				xmlRoomCodesData += xmlRoomCode + ",";
			}			
		}
		
		int csvRoomCount = mapData.size();
		if (csvRoomCount == xmlRoomCount) {
			result.setAttribute("Actual", "RoomCount in CSV = " + csvRoomCount + "\tRoomCodes : " + csvRoomCodesData + "\tRoomCount in xml = " + xmlRoomCount + "\tRoomCodes :" + xmlRoomCodesData);
			System.out.println("RoomCount in CSV = " + csvRoomCount + "\tRoomCodes : " + csvRoomCodesData + "\tRoomCount in xml = " + xmlRoomCount + "\tRoomCodes :" + xmlRoomCodesData);
		} else {
			result.setAttribute("Actual", "RoomCount in CSV = " + csvRoomCount + "\tRoomCodes : " + csvRoomCodesData + "\tRoomCount in xml = " + xmlRoomCount + "\tRoomCodes :" + xmlRoomCodesData);
			Assert.fail("Room count is not equal, RoomCount in CSV = " + csvRoomCount + "\tRoomCodes : " + csvRoomCodesData + "\tRoomCount in xml = " + xmlRoomCount + "\tRoomCodes :" + xmlRoomCodesData);
		}		
	}
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testRoomCount")
	public synchronized void testHotelName(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing hotel name");
		result.setAttribute("Expected", "Hotel name in csv and response xml should be same");
	
		Map<String,String[]> mapData = readCsvData(filePath);
		String xmlHotelName = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getName();
		
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			String csvRoomCode = entry.getKey();
			String[] data = entry.getValue();
			String csvHotelName = data[0];
			
			if(xmlHotelName.equals(csvHotelName)){
				result.setAttribute("Actual", "Hotel names are equal.");				
			}else{
				result.setAttribute("Actual", "hotelName in xml : " + xmlHotelName + "\thotelName in csv : " + csvHotelName);
				Assert.fail("Hotel names are not equal, " + "Xml hotelName( : " + xmlHotelName + "\t\tCsv hotelName("+csvRoomCode + ") : "  + csvHotelName);
			}
		}							
	}
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testRoomCount")
	public synchronized void testRoomType(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing room type");
		result.setAttribute("Expected", "Room type in csv and response xml should be same");
		
		TreeMap<String,String[]> mapData = readCsvData(filePath);
		TreeMap<String,String> mapXmlRoomTypes = new TreeMap<String, String>();
		TreeMap<String,String> mapCsvRoomTypes = new TreeMap<String, String>();
		
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			String roomType = room.getRoomType();
			String landOnly = roomType.substring(Math.max(roomType.length() - 3, 0));
			if(!landOnly.equals("L/O")){
				String xmlRoomType = room.getRoomTypeID();
				mapXmlRoomTypes.put(room.getRoomCode(), xmlRoomType);
			}			
		}
		
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			String[] data = entry.getValue();
			String csvRoomType = data[1].trim();
			mapCsvRoomTypes.put(entry.getKey(), csvRoomType);
		}
		
		String xmlRoomTypeData = "";
		String csvRoomTypeData = "";					
		for (Map.Entry<String, String> xmlEntry : mapXmlRoomTypes.entrySet()){
			String xmlRoomCode = xmlEntry.getKey();
			String xmlRoomType = xmlEntry.getValue();
			xmlRoomTypeData += xmlRoomCode + "_" + xmlRoomType + " | ";
			
			if (mapCsvRoomTypes.containsKey(xmlRoomCode)) {
				String csvRoomType = mapCsvRoomTypes.get(xmlRoomCode);
				csvRoomTypeData += xmlRoomCode + "_" + csvRoomType + " | ";
			}else{
				result.setAttribute("Actual", xmlRoomCode + " is not available in csv.");
				Assert.fail(xmlRoomCode + " is not available in csv.");
			}
		}
		
		if(xmlRoomTypeData.equals(csvRoomTypeData)){
			result.setAttribute("Actual", "Room types are equal.");
			System.out.println("Room types are equal. In xml : " + xmlRoomTypeData + "\tIn csv : " + csvRoomTypeData);
		}else{
			result.setAttribute("Actual", "Room types are not equal. In xml : " + xmlRoomTypeData + "\tIn csv : " + csvRoomTypeData);
			Assert.fail("Room types are not equal. In xml : " + xmlRoomTypeData + "\tIn csv : " + csvRoomTypeData);
		}
	}	
	
		
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testRoomCount")
	public synchronized void testBedType(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing bed type");
		result.setAttribute("Expected", "Bed type in csv and response xml should be same");
	
		TreeMap<String,String[]> mapData = readCsvData(filePath);
		TreeMap<String,String> mapXmlBedType = new TreeMap<String, String>();
		TreeMap<String,String> mapCsvBedType = new TreeMap<String, String>();
		
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			String roomType = room.getRoomType();
			String landOnly = roomType.substring(Math.max(roomType.length() - 3, 0));
			if(!landOnly.equals("L/O")){
				String xmlBedType = room.getBedTypeID();
				mapXmlBedType.put(room.getRoomCode(), xmlBedType);
			}
			
		}
		
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			String[] data = entry.getValue();
			String csvBedType = data[2].trim();
			mapCsvBedType.put(entry.getKey(), csvBedType);
		}
		
		String xmlBedTypeData = "";
		String csvBedTypeData = "";					
		for (Map.Entry<String, String> xmlEntry : mapXmlBedType.entrySet()){
			String xmlRoomCode = xmlEntry.getKey();
			String xmlBedType = xmlEntry.getValue();
			xmlBedTypeData += xmlRoomCode + "_" + xmlBedType + " | ";
			
			if (mapCsvBedType.containsKey(xmlRoomCode)) {
				String csvBedType = mapCsvBedType.get(xmlRoomCode);
				csvBedTypeData += xmlRoomCode + "_" + csvBedType + " | ";
			}else{
				result.setAttribute("Actual", xmlRoomCode + " is not available in csv.");
				Assert.fail(xmlRoomCode + " is not available in csv.");
			}
		}
		
		if(xmlBedTypeData.equals(csvBedTypeData)){
			result.setAttribute("Actual", "Bed type Ids are equal.");
			System.out.println("Bed type Ids are equal. In xml : " + xmlBedTypeData + "\tIn csv : " + csvBedTypeData);
		}else{
			result.setAttribute("Actual", "Bed type Ids are not equal. In xml : " + xmlBedTypeData + "\tIn csv : " + csvBedTypeData);
			Assert.fail("Bed type Ids are not equal. In xml : " + xmlBedTypeData + "\tIn csv : " + csvBedTypeData);
		}		
	}
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testRoomCount")
	public synchronized void testTotalRateAfterPromotion(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing total rate after promotion");
		result.setAttribute("Expected", "Total rate after promotion in csv and response shold be same");
		
		TreeMap<String,String[]> mapData = readCsvData(filePath);
		TreeMap<String,String> mapXmlRoomData = new TreeMap<String, String>();
		TreeMap<String,String> mapCsvRoomData = new TreeMap<String, String>();
		
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			String[] data = entry.getValue();
			String csvTotal = data[3].trim();
			mapCsvRoomData.put(entry.getKey(), csvTotal);
		}
		
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			String roomType = room.getRoomType();
			String landOnly = roomType.substring(Math.max(roomType.length() - 3, 0));
			if(!landOnly.equals("L/O")){
				RateplansInfo rateInfo = room.getRatesPlanInfo().entrySet().iterator().next().getValue();
				mapXmlRoomData.put(room.getRoomCode(), Double.toString(rateInfo.getTotalRate()));
			}			
		}		
			
		String xmlTotalData = "";
		String csvTotalData = "";		
		for (Map.Entry<String, String> xmlEntry : mapXmlRoomData.entrySet()){
			String xmlRoomCode = xmlEntry.getKey();
			double xmlTotal = Double.parseDouble(xmlEntry.getValue());
			xmlTotalData += xmlRoomCode + "_" + df.format(xmlTotal) + " | ";			
			
			if (mapCsvRoomData.containsKey(xmlRoomCode)) {
				double csvTotal = Double.parseDouble(mapCsvRoomData.get(xmlRoomCode));				
				csvTotalData += xmlRoomCode + "_" + df.format(csvTotal) + " | ";		
			}else{
				result.setAttribute("Actual", xmlRoomCode + " is not available in csv.");
				Assert.fail(xmlRoomCode + " is not available in csv.");
			}			
		}
		
		if(xmlTotalData.equals(csvTotalData)){
			result.setAttribute("Actual", "Total rates after promotion are equal.");
			System.out.println("Total rates after promotion are equal. In xml : " + xmlTotalData + "\tIn csv : " + csvTotalData);
		}else{
			result.setAttribute("Actual", "Total rates after promotion are not equal. In xml : " + xmlTotalData + "\tIn csv : " + csvTotalData);
			Assert.fail("Total rates after promotion are not equal. In xml : " + xmlTotalData + "\tIn csv : " + csvTotalData);
		}
	}
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testRoomCount")
	public synchronized void testDailyRatesAfterPromotion(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing daily rates after promotion");
		result.setAttribute("Expected", "Daily rates after promotion in csv and response should be same");
		
		TreeMap<String,String[]> mapData = readCsvData(filePath);
		TreeMap<String,ArrayList<String>> mapXmlDailyRates = new TreeMap<String, ArrayList<String>>();
		TreeMap<String,ArrayList<String>> mapCsvDailyRates = new TreeMap<String, ArrayList<String>>();
		
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			String roomType = room.getRoomType();
			String landOnly = roomType.substring(Math.max(roomType.length() - 3, 0));
			if(!landOnly.equals("L/O")){
				ArrayList<String> arrXmlDailyRates = new ArrayList<String>();
				RateplansInfo rateInfo = room.getRatesPlanInfo().entrySet().iterator().next().getValue();
				TreeMap<String, DailyRates> mapDailyRates = rateInfo.getDailyRates();
						
				for (Map.Entry<String, DailyRates> dailyRate : mapDailyRates.entrySet()){
					DailyRates rates = dailyRate.getValue();
					Double xmlDailyRates = rates.getTotal();
					arrXmlDailyRates.add(df.format(xmlDailyRates));					
					mapXmlDailyRates.put(room.getRoomCode(), arrXmlDailyRates);
				}
			}			
		}
		
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			String[] data = entry.getValue();
			String csvDailyRates = data[4];			
			ArrayList<String> arrCsvDailyRates = new ArrayList<String>(Arrays.asList(csvDailyRates.split("\\|")));
			ArrayList<String> arrDailyRates = new ArrayList<String>();
			for(String dailyRate : arrCsvDailyRates){				
				arrDailyRates.add(df.format(Double.parseDouble(dailyRate)));
			}
			mapCsvDailyRates.put(entry.getKey(), arrDailyRates);
		}
		
		ArrayList<String> arrxmlDailyRate = new ArrayList<String>();
		ArrayList<String> arrcsvDailyRate = new ArrayList<String>();		
		String xmlDailyRateData = "";
		String csvDailyRateData = "";
		
		for (Map.Entry<String, ArrayList<String>> xmlEntry : mapXmlDailyRates.entrySet()){
			String xmlRoomCode = xmlEntry.getKey();
			arrxmlDailyRate = xmlEntry.getValue();	
			xmlDailyRateData += xmlRoomCode + "_" + arrxmlDailyRate + " | ";
			
			if (mapCsvDailyRates.containsKey(xmlRoomCode)) {
				arrcsvDailyRate = mapCsvDailyRates.get(xmlRoomCode);	
				csvDailyRateData += xmlRoomCode + "_" + arrcsvDailyRate + " | ";
			}else{
				result.setAttribute("Actual", xmlRoomCode + " is not available in csv.");
				Assert.fail(xmlRoomCode + " is not available in csv.");
			}			
		}		
		
		if(xmlDailyRateData.equals(csvDailyRateData)){
			result.setAttribute("Actual", "Daily rates after promotion are equal.");
			System.out.println("Daily rates after promotion are equal. In xml : " + xmlDailyRateData + "\tIn csv : " + csvDailyRateData);
		}else{
			result.setAttribute("Actual", "Daily rates after promotion are not equal. In xml : " + xmlDailyRateData + "\tIn csv : " + csvDailyRateData);
			Assert.fail("Daily rates are not equal, In xml : " + xmlDailyRateData + "\tIn csv : " + csvDailyRateData);
		}		
	}
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testRoomCount")
	public synchronized void testPromotionCode(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing promotion code");
		result.setAttribute("Expected", "Promotion code in csv and response xml should be same");
		
		TreeMap<String,String[]> mapData = readCsvData(filePath);
		TreeMap<String,String> mapXmlPromoCodes = new TreeMap<String, String>();
		TreeMap<String,String> mapCsvPromoCodes = new TreeMap<String, String>();
		
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			String roomType = room.getRoomType();
			String landOnly = roomType.substring(Math.max(roomType.length() - 3, 0));
			if(!landOnly.equals("L/O")){
				String xmlPromoCode = room.getPromotionCode();
				mapXmlPromoCodes.put(room.getRoomCode(), xmlPromoCode);
			}			
		}
		
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			String[] data = entry.getValue();
			String csvPromoCode = data[5].trim();
			mapCsvPromoCodes.put(entry.getKey(), csvPromoCode);
		}
		
		String xmlPromoCodeData = "";
		String csvPromoCodeData = "";				
		for (Map.Entry<String, String> xmlEntry : mapXmlPromoCodes.entrySet()){
			String xmlRoomCode = xmlEntry.getKey();
			String xmlPromoCode = xmlEntry.getValue();
			xmlPromoCodeData += xmlRoomCode + "_" + xmlPromoCode + " | ";
			
			if (mapCsvPromoCodes.containsKey(xmlRoomCode)) {
				String csvPromoCodes = mapCsvPromoCodes.get(xmlRoomCode);
				csvPromoCodeData += xmlRoomCode + "_" + csvPromoCodes + " | ";
			}else{
				result.setAttribute("Actual", xmlRoomCode + " is not available in csv.");
				Assert.fail(xmlRoomCode + " is not available in csv.");
			}
		}
		
		if(xmlPromoCodeData.equals(csvPromoCodeData)){
			result.setAttribute("Actual", "Promotion codes are equal.");
			System.out.println("Promotion codes are equal. In xml : " + xmlPromoCodeData + "\tIn csv : " + csvPromoCodeData);
		}else{
			result.setAttribute("Actual", "Promotion codes are not equal. In xml : " + xmlPromoCodeData + "\tIn csv : " + csvPromoCodeData);
			Assert.fail("Promotion codes are not equal. In xml : " + xmlPromoCodeData + "\tIn csv : " + csvPromoCodeData);
		}
	}
	
			
	public TreeMap<String,String[]> readCsvData(String filePath) {
		boolean withHeader = true;
		String hotelCode  = AvailRequest.getCode()[0];
		String adultCount = AvailRequest.getRoomlist().get(0).getAdultsCount();
		String childCount = AvailRequest.getRoomlist().get(0).getChildCount();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
		Date CheckIndate = null;
		Date CheckOutdate = null;			
		
		try {
				CheckIndate = sdf.parse(AvailRequest.getCheckin());
				CheckOutdate = sdf.parse(AvailRequest.getCheckout());				
		} catch (ParseException e) {
				e.printStackTrace();
		}
		String checkIn = sdf.format(CheckIndate);
		String checkOut = sdf.format(CheckOutdate);		
		
		String path = filePath.split("\\.csv")[0];
		String selectedCsvVersion = path.substring(Math.max(path.length() - 2, 0));
		TreeMap<String,String[]> mapData =null;
			
		if (selectedCsvVersion.equalsIgnoreCase("V3")) {
			CsvReader reader = new CsvReader();			
			
			try {
				CSVParser parser = reader.csvRead(filePath, withHeader);
				mapData = new TreeMap<String, String[]>();
				
				for (CSVRecord record : parser) {
					if (record.get("BuildingID").equals(hotelCode) && record.get("StartDate").equals(checkIn) && record.get("EndDate").equals(checkOut) 
						 && record.get("Adults").equals(adultCount) && record.get("Children").equals(childCount)) {
						
						String roomCode = record.get("roomCode");
						String hotelName = record.get("BuildingName");
						String roomType = record.get("RoomTypeId");
						String bedType = record.get("bedTypeID");
						String totalRate = record.get("Total Rate After Promotion");
						String dailyRates = record.get("Daily Rates After Promotion");
						String promoCode = record.get("Promotion Code");
						String csvhotelCode = record.get("BuildingID");
						
						mapData.put(roomCode, new String[] {hotelName,roomType,bedType,totalRate,dailyRates,promoCode,csvhotelCode});						
					}				
				}				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}else{
			System.out.println("Please enter a CSV in V3. Entered csv is in " + selectedCsvVersion + " format.");
		}
		
		return mapData;		
	}
	

	public AvailabilityRequest getAvailabilityData() throws Exception {
		DataLoader loader = new DataLoader();
		String data[][] = ExcelDataSingleton.getInstance("Resources/CSV_TestingChecklist.xls","Scenario").getDataHolder();
		String scenario = data[14][0];
		
		if((scenario == null || scenario.isEmpty()) || scenario.substring(Math.max(scenario.length() - 9, 0)).equals("scenarios")){
			AvailRequest = null;
		}else {
			AvailRequest = loader.getReservationReqObjList(data)[14][0];
		}
				
		return AvailRequest;		
	}

}
