package com.rezgateway.automation.bonotel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.rezgateway.automation.JavaHttpHandler;
import com.rezgateway.automation.builder.request.AvailabilityRequestBuilder;
import com.rezgateway.automation.input.CsvReader;
import com.rezgateway.automation.pojo.AvailabilityRequest;
import com.rezgateway.automation.pojo.AvailabilityResponse;
import com.rezgateway.automation.pojo.DailyRates;
import com.rezgateway.automation.pojo.HttpResponse;
import com.rezgateway.automation.pojo.RateplansInfo;
import com.rezgateway.automation.pojo.Room;
import com.rezgateway.automation.reader.response.AvailabilityResponseReader;
import com.rezgateway.automation.reports.ExtentTestNGReportBuilderExt;
import com.rezgateway.automation.xmlout.utill.DataLoader;
import com.rezgateway.automation.xmlout.utill.ExcelDataSingleton;

public class CSV_TC_001 extends ExtentTestNGReportBuilderExt {
	AvailabilityResponse AvailResponse = new AvailabilityResponse();
	AvailabilityRequest AvailRequest = new AvailabilityRequest();	
	
	@Parameters("TestUrl")
	@Test(priority = 0)
	public synchronized void availbilityTest(String TestUrl) throws Exception {
		getAvailabilityData();
		
		String Scenario     = AvailRequest.getScenarioID();
		String HotelCode    = Arrays.toString(AvailRequest.getCode());
		String SearchString = AvailRequest.getCheckin()+"|"+AvailRequest.getCheckout()+" |"+AvailRequest.getNoOfRooms()+"R| "+AvailRequest.getUserName()+" |"+AvailRequest.getPassword();
		
		ITestResult result = Reporter.getCurrentTestResult();
		String testname = "Test Scenario:" + Scenario + " : Search By : " + AvailRequest.getSearchType() + " Code : " + HotelCode+ "Criteria : "+SearchString;
		
		result.setAttribute("TestName", testname);
		result.setAttribute("Expected", "Results Should be available");

		JavaHttpHandler handler = new JavaHttpHandler();
		HttpResponse Response = handler.sendPOST(TestUrl, "xml=" + new AvailabilityRequestBuilder().buildRequest("Resources/RegressionSuite_AvailRequest_senarioID_" + AvailRequest.getScenarioID() + ".xml", AvailRequest));
		System.out.println("Response : " + Response.toString());
		if(Response.getRESPONSE_CODE() == 200) {
			AvailResponse = new AvailabilityResponseReader().getResponse(Response.getRESPONSE());
						
			if(AvailResponse.getHotelCount() > 0) {
				result.setAttribute("Actual", "Results Available, Hotel Count :" + AvailResponse.getHotelCount());
			}else {
				result.setAttribute("Actual", "Results not available Error Code :" + AvailResponse.getErrorCode() + " Error Desc :" + AvailResponse.getErrorDescription());
				Assert.fail("No Results Error Code :" + AvailResponse.getErrorCode());
			}
		}else {
			result.setAttribute("Actual", "No Response recieved Code :" + Response.getRESPONSE_CODE());
			Assert.fail("Invalid Response Code :" + Response.getRESPONSE_CODE() + " ,No Response received");
		}	
	}
	
	@Test(dependsOnMethods = "availbilityTest")
	public synchronized void testHotelCode() {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing hotel code");
		result.setAttribute("Expected", "Hotel code in availability request and response should be same");
		
		if (AvailResponse.getHotelList().containsKey(AvailRequest.getCode()[0])) {
			result.setAttribute("Actual", AvailResponse.getHotelList().entrySet().iterator().next().getKey());			
		} else {
			result.setAttribute("Actual", "Requested Hotel code (" + AvailRequest.getCode()[0] + ")  is not available ,Hotel Code is " + AvailResponse.getHotelList().entrySet().iterator().next().getKey());
			Assert.fail("Requested Hotel code is not available");
		}
	}	
	
	@Test(dependsOnMethods = "testHotelCode")
	public synchronized void testCsvDataAvailability() {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing Availability of csv record");
		result.setAttribute("Expected", "Data should be available according to the search criteria");
				
		if(readCsvData().size()>0){
			result.setAttribute("Actual", "Record count is  " + readCsvData().size());
		}else{
			result.setAttribute("Actual", "Record count is  " + readCsvData().size());
			Assert.fail("No records available in csv");
		}		
	}
	
	@Test(dependsOnMethods = "testCsvDataAvailability")
	public synchronized void testTotalRateAfterPromotion() {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing total rate after promotion");
		result.setAttribute("Expected", "Total rate after promotion in csv and response shold be same");
		
		Map<String,String[]> mapData = readCsvData();
		
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			for (Map.Entry<String, String[]> entry : mapData.entrySet()){
				String csvRoomCode = entry.getKey();
				String[] data = entry.getValue();
				double csvTotal = Double.parseDouble(data[0]);
				
				if(room.getRoomCode().equals(csvRoomCode)){
					RateplansInfo rateInfo = room.getRatesPlanInfo().entrySet().iterator().next().getValue();
					double xmlTotal = rateInfo.getTotalRate();
					if(xmlTotal == csvTotal){
						result.setAttribute("Actual", "Total rates after promotion are equal" + "Xml rate(" + room.getRoomCode() + ") : " + xmlTotal + "\t\tCsv rate("+csvRoomCode + ") : "  + csvTotal);
					}else{
						result.setAttribute("Actual", "Rate in xml : " + xmlTotal + "\tRate in csv : " + csvTotal);
						Assert.fail("Total rates are not equal, " + "Xml rate(" + room.getRoomCode() + ") : " + xmlTotal + "\t\tCsv rate("+csvRoomCode + ") : "  + csvTotal);
					}					
				}
			}			
		}
	}
	
	@Test(dependsOnMethods = "testCsvDataAvailability")
	public synchronized void testDailyRatesAfterPromotion() {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing daily rates after promotion");
		result.setAttribute("Expected", "Daily rates after promotion in csv and response shold be same");
		
		Map<String,String[]> mapData = readCsvData();
		
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			for (Map.Entry<String, String[]> entry : mapData.entrySet()){
				String csvRoomCode = entry.getKey();
				String[] data = entry.getValue();
				String csvDailyRates = data[1];
				ArrayList<String> arrCsvDailyRates = new ArrayList<String>(Arrays.asList(csvDailyRates.split("\\|")));
				
				ArrayList<String> arrXmlDailyRates = new ArrayList<String>();
				if(room.getRoomCode().equals(csvRoomCode)){
					RateplansInfo rateInfo = room.getRatesPlanInfo().entrySet().iterator().next().getValue();
					TreeMap<String, DailyRates> mapDailyRates = rateInfo.getDailyRates();
					
					for (Map.Entry<String, DailyRates> dailyRate : mapDailyRates.entrySet()){
						DailyRates rates = dailyRate.getValue();
						String xmlDailyRates = Double.toString(rates.getTotal());
						arrXmlDailyRates.add(xmlDailyRates);						
					}
					
					if(Arrays.equals(arrXmlDailyRates.toArray(), arrCsvDailyRates.toArray())){
						result.setAttribute("Actual", "Daily rates after promotion are equal" + "Xml rates(" + room.getRoomCode() + ") : " + arrXmlDailyRates + "\t\tCsv rates("+csvRoomCode + ") : "  + arrCsvDailyRates);
					}else{
						result.setAttribute("Actual", "Rates in xml : " + arrXmlDailyRates.toString() + "\tRate in csv : " + arrCsvDailyRates.toString());
						Assert.fail("Daily rates are not equal, " + "Xml rates(" + room.getRoomCode() + ") : " + arrXmlDailyRates + "\t\tCsv rates("+csvRoomCode + ") : "  + arrCsvDailyRates);
					}
				}
			}			
		}
	}
	
	@Test(dependsOnMethods = "testCsvDataAvailability")
	public synchronized void testCancellationPolicy() {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing cancellation policy");
		result.setAttribute("Expected", "Cancellation policy in csv and response shold be same");
	}
	
/*	@Test(dependsOnMethods = "availbilityTest")
	public synchronized void getData() {		
		Map<String,String[]> mapData = readCsvData();		
		
		for (Map.Entry<String, String[]> entry : mapData.entrySet()) {
			String[] data = entry.getValue();								
			System.out.println("RoomCode : " + entry.getKey() + "\tTotal : " + data[0] + "\tDailyRates :" + data[1] + "\tCp : " + data[2]);
						
		}		
	}*/
	
	
	public AvailabilityRequest getAvailabilityData() throws Exception {
		
		DataLoader loader = new DataLoader();
		AvailRequest = loader.getReservationReqObjList(ExcelDataSingleton.getInstance("Resources/Full Regression Testing Checklist.xls","Scenario").getDataHolder())[0][0];
		return AvailRequest;

	}	
	
	public Map<String,String[]> readCsvData() {
		String filePath = "/home/harshani/Desktop/CSV/RateExport_181127_1812V4.csv"; // with header
		String filePath1 = "/home/harshani/Desktop/CSV/tariffdailyreport_181015_0_V2.csv"; //without header
		boolean isHeader = true;
		String hotelCode  = AvailRequest.getCode()[0];
		String adultCount = AvailRequest.getRoomlist().get(0).getAdultsCount();
		String childCount = AvailRequest.getRoomlist().get(0).getChildCount();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
		Date CheckIndate = null;
		Date CheckOutdate = null;		
			
			try {
				CheckIndate = sdf.parse(AvailRequest.getCheckin());
				CheckOutdate = sdf.parse(AvailRequest.getCheckout());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			String checkIn = sdf.format(CheckIndate);
			String checkOut = sdf.format(CheckOutdate);				
		
		CsvReader reader = new CsvReader();
		Map<String,String[]> mapData =null;
		try {
			CSVParser parser = reader.csvRead(filePath, isHeader);
			mapData = new HashMap<String, String[]>();
			
			for (CSVRecord record : parser) {
				if (record.get("hotelCode").equals(hotelCode) && record.get("StartDate").equals(checkIn) && record.get("EndDate").equals(checkOut) 
					 && record.get("adults").equals(adultCount) && record.get("children").equals(childCount)) {
					
					String roomCode = record.get("roomCode");
					String totalRate = record.get("totalRateAfterPromotion");
					String dailyRates = record.get("dailyRatesAfterPromotion");
					String cp = record.get("cancelPolicy");
					
					mapData.put(roomCode, new String[] {totalRate,dailyRates,cp});						
				}				
			}				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapData;
		
	}

}
