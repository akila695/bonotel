package com.rezgateway.automation.bonotel;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.rezgateway.automation.JavaHttpHandler;
import com.rezgateway.automation.builder.request.AvailabilityRequestBuilder;
import com.rezgateway.automation.input.CsvReader;
import com.rezgateway.automation.pojo.AvailabilityRequest;
import com.rezgateway.automation.pojo.AvailabilityResponse;
import com.rezgateway.automation.pojo.HttpResponse;
import com.rezgateway.automation.pojo.RateplansInfo;
import com.rezgateway.automation.pojo.Room;
import com.rezgateway.automation.reader.response.AvailabilityResponseReader;
import com.rezgateway.automation.reports.ExtentTestNGReportBuilderExt;
import com.rezgateway.automation.xmlout.utill.DataLoader;
import com.rezgateway.automation.xmlout.utill.ExcelDataSingleton;

public class CSV_V1_TC_003 extends ExtentTestNGReportBuilderExt {
	AvailabilityResponse AvailResponse = new AvailabilityResponse();
	AvailabilityRequest AvailRequest = new AvailabilityRequest();	
	DecimalFormat df = new DecimalFormat("#.00");
	
	@Parameters({"TestUrl", "CsvPath"})
	@Test(priority = 0)
	public synchronized void availbilityTest(String TestUrl, String filePath) throws Exception {
		ITestResult result = Reporter.getCurrentTestResult();
		String Scenario     = AvailRequest.getScenarioID();
		String HotelCode    = Arrays.toString(AvailRequest.getCode());
		String SearchString = AvailRequest.getCheckin()+"|"+AvailRequest.getCheckout()+" |"+AvailRequest.getNoOfRooms()+"R| "+AvailRequest.getUserName()+" |"+AvailRequest.getPassword();
		String testname = "Test Scenario:" + Scenario + " : Search By : " + AvailRequest.getSearchType() + " Code : " + HotelCode+ "Criteria : "+SearchString;
		
		if (getAvailabilityData(filePath) == null) {
			result.setAttribute("Actual", "Scenario id :" + Scenario);
			Assert.fail("Search criteria has not mentioned...");
		} else {
			getAvailabilityData(filePath);		
			
			result.setAttribute("TestName", testname);
			result.setAttribute("Expected", "Results Should be available");

			JavaHttpHandler handler = new JavaHttpHandler();
			HttpResponse Response = handler.sendPOST(TestUrl, "xml=" + new AvailabilityRequestBuilder().buildRequest("Resources/CsvSuite_AvailRequest_senarioID_" + AvailRequest.getScenarioID() + ".xml", AvailRequest));
			
			System.out.println("xml=" + new AvailabilityRequestBuilder().buildRequest("Resources/CsvSuite_AvailRequest_senarioID_" + AvailRequest.getScenarioID() + ".xml", AvailRequest));
			System.out.println(Response.getRESPONSE());
			
			if(Response.getRESPONSE_CODE() == 200) {
				AvailResponse = new AvailabilityResponseReader().getResponse(Response.getRESPONSE());
							
				if(AvailResponse.getHotelCount() > 0) {
					result.setAttribute("Actual", "Results Available, Hotel Count :" + AvailResponse.getHotelCount());
				}else {
					result.setAttribute("Actual", "Results not available Error Code :" + AvailResponse.getErrorCode() + " Error Desc :" + AvailResponse.getErrorDescription());
					Assert.fail("No Results Error Code :" + AvailResponse.getErrorCode());
				}
			}else {
				result.setAttribute("Actual", "No Response recieved Code :" + Response.getRESPONSE_CODE());
				Assert.fail("Invalid Response Code :" + Response.getRESPONSE_CODE() + " ,No Response received");
			}	
		}	
	}

	
	@Test(dependsOnMethods = "availbilityTest")
	public synchronized void testHotelCode() {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing hotel code");
		result.setAttribute("Expected", "Hotel code in availability request and response should be same");
		
		if (AvailResponse.getHotelList().containsKey(AvailRequest.getCode()[0])) {
			result.setAttribute("Actual", AvailResponse.getHotelList().entrySet().iterator().next().getKey());			
		} else {
			result.setAttribute("Actual", "Requested Hotel code (" + AvailRequest.getCode()[0] + ")  is not available ,Hotel Code is " + AvailResponse.getHotelList().entrySet().iterator().next().getKey());
			Assert.fail("Requested Hotel code is not available");
		}
	}	
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testHotelCode")
	public synchronized void testCsvDataAvailability(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing Availability of csv record");
		result.setAttribute("Expected", "Data should be available according to the search criteria");
		
		Map<String,String[]> mapData = readCsvData(filePath);
		if(mapData == null){
			result.setAttribute("Actual", "Issue in csv file." );
			Assert.fail("Issue in csv file.");
		}else if(mapData.size()>0){
			result.setAttribute("Actual", "Record count is  " + readCsvData(filePath).size());
		}else{
			result.setAttribute("Actual", "Record count is  " + readCsvData(filePath).size());
			Assert.fail("No records available in csv");
		}				
	}
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testCsvDataAvailability")
	public synchronized void testRoomCount(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing room count");
		result.setAttribute("Expected", "Room count in csv and response xml should be same");
		
		String xmlRoomCodesData = "";
		String csvRoomCodesData = "";
		
		Map<String,String[]> mapData = readCsvData(filePath);
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			csvRoomCodesData += entry.getKey() + ",";
		}
		
		int xmlRoomCount = 0;
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			String roomType = room.getRoomType();
			String landOnly = roomType.substring(Math.max(roomType.length() - 3, 0));
			if(!landOnly.equals("L/O")){
				xmlRoomCount++;
				String xmlRoomCode = room.getRoomCode();
				xmlRoomCodesData += xmlRoomCode + ",";
			}			
		}		
		 
		int csvRoomCount = mapData.size();
		if (csvRoomCount == xmlRoomCount) {
			result.setAttribute("Actual", "RoomCount in CSV = " + csvRoomCount + "\tRoomCodes : " + csvRoomCodesData + "\tRoomCount in xml = " + xmlRoomCount + "\tRoomCodes :" + xmlRoomCodesData);
			System.out.println("RoomCount in CSV = " + csvRoomCount + "\tRoomCodes : " + csvRoomCodesData + "\tRoomCount in xml = " + xmlRoomCount + "\tRoomCodes :" + xmlRoomCodesData);
		} else {
			result.setAttribute("Actual", "RoomCount in CSV = " + csvRoomCount + "\tRoomCodes : " + csvRoomCodesData + "\tRoomCount in xml = " + xmlRoomCount + "\tRoomCodes :" + xmlRoomCodesData);
			Assert.fail("Room count is not equal, RoomCount in CSV = " + csvRoomCount + "\tRoomCodes : " + csvRoomCodesData + "\tRoomCount in xml = " + xmlRoomCount + "\tRoomCodes :" + xmlRoomCodesData);
		}		
	}
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testRoomCount")
	public synchronized void testHotelName(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing hotel name");
		result.setAttribute("Expected", "Hotel name in csv and response xml should be same");
	
		Map<String,String[]> mapData = readCsvData(filePath);
		String xmlHotelName = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getName();
		
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			String csvRoomCode = entry.getKey();
			String[] data = entry.getValue();
			String csvHotelName = data[0];
			
			if(xmlHotelName.equals(csvHotelName)){
				result.setAttribute("Actual", "Hotel names are equal.");				
			}else{
				result.setAttribute("Actual", "hotelName in xml : " + xmlHotelName + "\thotelName in csv : " + csvHotelName);
				Assert.fail("Hotel names are not equal, " + "Xml hotelName( : " + xmlHotelName + "\t\tCsv hotelName("+csvRoomCode + ") : "  + csvHotelName);
			}
		}							
	}
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testRoomCount")
	public synchronized void testRoomType(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing room type");
		result.setAttribute("Expected", "Room type in csv and response xml should be same");
		
		TreeMap<String,String[]> mapData = readCsvData(filePath);
		TreeMap<String,String> mapXmlRoomTypes = new TreeMap<String, String>();
		TreeMap<String,String> mapCsvRoomTypes = new TreeMap<String, String>();
		
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			String roomType = room.getRoomType();
			String landOnly = roomType.substring(Math.max(roomType.length() - 3, 0));
			if(!landOnly.equals("L/O")){
				String xmlRoomType = room.getRoomTypeID();
				mapXmlRoomTypes.put(room.getRoomCode(), xmlRoomType);
			}			
		}
		
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			String[] data = entry.getValue();
			String csvRoomType = data[1].trim();
			mapCsvRoomTypes.put(entry.getKey(), csvRoomType);
		}
		
		String xmlRoomTypeData = "";
		String csvRoomTypeData = "";					
		for (Map.Entry<String, String> xmlEntry : mapXmlRoomTypes.entrySet()){
			String xmlRoomCode = xmlEntry.getKey();
			String xmlRoomType = xmlEntry.getValue();
			xmlRoomTypeData += xmlRoomCode + "_" + xmlRoomType + " | ";
			
			if (mapCsvRoomTypes.containsKey(xmlRoomCode)) {
				String csvRoomType = mapCsvRoomTypes.get(xmlRoomCode);
				csvRoomTypeData += xmlRoomCode + "_" + csvRoomType + " | ";
			}else{
				result.setAttribute("Actual", xmlRoomCode + " is not available in csv.");
				Assert.fail(xmlRoomCode + " is not available in csv.");
			}
		}
		
		if(xmlRoomTypeData.equals(csvRoomTypeData)){
			result.setAttribute("Actual", "Room types are equal.");
			System.out.println("Room types are equal. In xml : " + xmlRoomTypeData + "\tIn csv : " + csvRoomTypeData);
		}else{
			result.setAttribute("Actual", "Room types are not equal. In xml : " + xmlRoomTypeData + "\tIn csv : " + csvRoomTypeData);
			Assert.fail("Room types are not equal. In xml : " + xmlRoomTypeData + "\tIn csv : " + csvRoomTypeData);
		}
	}
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testRoomCount")
	public synchronized void testRatePlanCode(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing rate plan code ");
		result.setAttribute("Expected", "Rate plan code in csv and response xml should be same");
		
		TreeMap<String,String[]> mapData = readCsvData(filePath);
		TreeMap<String,String> mapXmlRateCode = new TreeMap<String, String>();
		TreeMap<String,String> mapCsvRateCode = new TreeMap<String, String>();
		
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			String roomType = room.getRoomType();
			String landOnly = roomType.substring(Math.max(roomType.length() - 3, 0));
			if(!landOnly.equals("L/O")){
				RateplansInfo rateInfo = room.getRatesPlanInfo().entrySet().iterator().next().getValue();
				String xmlRateCode = rateInfo.getRatePlanCode();
				mapXmlRateCode.put(room.getRoomCode(), xmlRateCode);
			}			
		}
		
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			String[] data = entry.getValue();
			String csvRateCode = data[2].trim();
			mapCsvRateCode.put(entry.getKey(), csvRateCode);
		}
		
		String xmlRateCodeData = "";
		String csvRateCodeData = "";					
		for (Map.Entry<String, String> xmlEntry : mapXmlRateCode.entrySet()){
			String xmlRoomCode = xmlEntry.getKey();
			String xmlRateCode = xmlEntry.getValue();
			xmlRateCodeData += xmlRoomCode + "_" + xmlRateCode + " | ";
			
			if (mapCsvRateCode.containsKey(xmlRoomCode)) {
				String csvRateCode = mapCsvRateCode.get(xmlRoomCode);
				csvRateCodeData += xmlRoomCode + "_" + csvRateCode + " | ";
			}else{
				result.setAttribute("Actual", xmlRoomCode + " is not available in csv.");
				Assert.fail(xmlRoomCode + " is not available in csv.");
			}
		}
		
		if(xmlRateCodeData.equals(csvRateCodeData)){
			result.setAttribute("Actual", "Rate plan codes are equal.");
			System.out.println("Rate plan codes are equal. In xml : " + xmlRateCodeData + "\tIn csv : " + csvRateCodeData);
		}else{
			result.setAttribute("Actual", "Rate plan codes are not equal. In xml : " + xmlRateCodeData + "\tIn csv : " + csvRateCodeData);
			Assert.fail("Rate plan codes are not equal. In xml : " + xmlRateCodeData + "\tIn csv : " + csvRateCodeData);
		}
	}
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testRoomCount")
	public synchronized void testBedType(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing bed type");
		result.setAttribute("Expected", "Bed type in csv and response xml should be same");
	
		TreeMap<String,String[]> mapData = readCsvData(filePath);
		TreeMap<String,String> mapXmlBedType = new TreeMap<String, String>();
		TreeMap<String,String> mapCsvBedType = new TreeMap<String, String>();
		
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			String roomType = room.getRoomType();
			String landOnly = roomType.substring(Math.max(roomType.length() - 3, 0));
			if(!landOnly.equals("L/O")){
				String xmlBedType = room.getBedTypeID();
				mapXmlBedType.put(room.getRoomCode(), xmlBedType);
			}			
		}
		
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			String[] data = entry.getValue();
			String csvBedType = data[3].trim();
			mapCsvBedType.put(entry.getKey(), csvBedType);
		}
		
		String xmlBedTypeData = "";
		String csvBedTypeData = "";					
		for (Map.Entry<String, String> xmlEntry : mapXmlBedType.entrySet()){
			String xmlRoomCode = xmlEntry.getKey();
			String xmlBedType = xmlEntry.getValue();
			xmlBedTypeData += xmlRoomCode + "_" + xmlBedType + " | ";
			
			if (mapCsvBedType.containsKey(xmlRoomCode)) {
				String csvBedType = mapCsvBedType.get(xmlRoomCode);
				csvBedTypeData += xmlRoomCode + "_" + csvBedType + " | ";
			}else{
				result.setAttribute("Actual", xmlRoomCode + " is not available in csv.");
				Assert.fail(xmlRoomCode + " is not available in csv.");
			}
		}
		
		if(xmlBedTypeData.equals(csvBedTypeData)){
			result.setAttribute("Actual", "Bed type Ids are equal.");
			System.out.println("Bed type Ids are equal. In xml : " + xmlBedTypeData + "\tIn csv : " + csvBedTypeData);
		}else{
			result.setAttribute("Actual", "Bed type Ids are not equal. In xml : " + xmlBedTypeData + "\tIn csv : " + csvBedTypeData);
			Assert.fail("Bed type Ids are not equal. In xml : " + xmlBedTypeData + "\tIn csv : " + csvBedTypeData);
		}		
	}
	
	
	@Parameters("CsvPath")
	@Test(dependsOnMethods = "testRoomCount")
	public synchronized void testTotalRateAfterPromotion(String filePath) {
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Testing total rate after promotion");
		result.setAttribute("Expected", "Total rate after promotion in csv and response shold be same");
		
		TreeMap<String,String[]> mapData = readCsvData(filePath);
		TreeMap<String,String> mapXmlRoomData = new TreeMap<String, String>();
		TreeMap<String,String> mapCsvRoomData = new TreeMap<String, String>();
		
		for (Map.Entry<String, String[]> entry : mapData.entrySet()){
			String[] data = entry.getValue();
			String csvTotal = data[4].trim();
			mapCsvRoomData.put(entry.getKey(), csvTotal);
		}
		
		Map<String, ArrayList<Room>> roomInfo = AvailResponse.getHotelList().entrySet().iterator().next().getValue().getRoomInfo();
		for (Room room : roomInfo.entrySet().iterator().next().getValue()) {
			String roomType = room.getRoomType();
			String landOnly = roomType.substring(Math.max(roomType.length() - 3, 0));
			if(!landOnly.equals("L/O")){
				RateplansInfo rateInfo = room.getRatesPlanInfo().entrySet().iterator().next().getValue();
				mapXmlRoomData.put(room.getRoomCode(), Double.toString(rateInfo.getTotalRate()));
			}			
		}		
			
		String xmlTotalData = "";
		String csvTotalData = "";		
		for (Map.Entry<String, String> xmlEntry : mapXmlRoomData.entrySet()){
			String xmlRoomCode = xmlEntry.getKey();
			double xmlTotal = Double.parseDouble(xmlEntry.getValue());
			xmlTotalData += xmlRoomCode + "_" + df.format(xmlTotal) + " | ";			
			
			if (mapCsvRoomData.containsKey(xmlRoomCode)) {
				double csvTotal = Double.parseDouble(mapCsvRoomData.get(xmlRoomCode));				
				csvTotalData += xmlRoomCode + "_" + df.format(csvTotal) + " | ";		
			}else{
				result.setAttribute("Actual", xmlRoomCode + " is not available in csv.");
				Assert.fail(xmlRoomCode + " is not available in csv.");
			}			
		}
		
		if(xmlTotalData.equals(csvTotalData)){
			result.setAttribute("Actual", "Total rates after promotion are equal.");
			System.out.println("Total rates after promotion are equal. In xml : " + xmlTotalData + "\tIn csv : " + csvTotalData);
		}else{
			result.setAttribute("Actual", "Total rates after promotion are not equal. In xml : " + xmlTotalData + "\tIn csv : " + csvTotalData);
			Assert.fail("Total rates after promotion are not equal. In xml : " + xmlTotalData + "\tIn csv : " + csvTotalData);
		}
	}	

	
	public TreeMap<String,String[]> readCsvData(String filePath) {
		boolean withHeader = false;
		CsvReader reader = new CsvReader();
		String hotelCode  = AvailRequest.getCode()[0];
		String bedType = AvailRequest.getRoomlist().get(0).getBedTypeID();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
		Date CheckIndate = null;		
		
		try {
				CheckIndate = sdf.parse(AvailRequest.getCheckin());			
		} catch (ParseException e) {
				e.printStackTrace();
		}
		String checkIn = sdf.format(CheckIndate);		
		TreeMap<String,String[]> mapData =null;
		
		try {
			CSVParser parser = reader.csvRead(filePath, withHeader);
			mapData = new TreeMap<String, String[]>();
				
			for (CSVRecord record : parser) {
				if (record.get(1).equals(hotelCode) && record.get(9).equals(checkIn) && record.get(5).equals(bedType)) {
						
					String roomCode = record.get(2);
					String hotelName = record.get(0);
					String roomType = record.get(3);
					String rateCode = record.get(7);
					String bedTypeCsv = record.get(5);
					String totalRate = record.get(11);
					String csvhotelCode = record.get(1);
						
					mapData.put(roomCode, new String[] {hotelName,roomType,rateCode,bedTypeCsv,totalRate,csvhotelCode});						
				}				
			}				
		} catch (Exception e) {
				e.printStackTrace();
		}
		
		return mapData;		
	}
	

	public AvailabilityRequest getAvailabilityData(String filePath) throws Exception {
		boolean withHeader = false;		
		String[] hotelCode = new String[1];
		String bedType = null;
		Date checkIn = null;
		Date checkOut = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy");
		
		String path = filePath.split("\\.csv")[0];
		String selectedCsvVersion = path.substring(Math.max(path.length() - 2, 0));
		
		if (selectedCsvVersion.equalsIgnoreCase("V1")) {			
			CsvReader reader = new CsvReader();	
			try {
				CSVParser parser = reader.csvRead(filePath, withHeader);
				
				Random rand = new Random();
				int recordNo = rand.nextInt(500000) + 1;
				System.out.println("Record no : " + recordNo);
				
				int currentIndex = 0;
				for (CSVRecord record : parser) {
					currentIndex++;
					if(currentIndex == recordNo){
						hotelCode[0] = record.get(1);
						bedType = record.get(5);
						checkIn = sdf.parse(record.get(9));							
					}							
				}				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			System.out.println("Please enter a CSV in V1. Entered csv is in " + selectedCsvVersion + " format.");
		}		
		
		DataLoader loader = new DataLoader();
		String data[][] = ExcelDataSingleton.getInstance("Resources/CSV_TestingChecklist.xls","Scenario").getDataHolder();
		String scenario = data[3][0];
		
		if((scenario == null || scenario.isEmpty()) || scenario.substring(Math.max(scenario.length() - 9, 0)).equals("scenarios")){
			AvailRequest = null;
		}else {
			AvailRequest = loader.getReservationReqObjList(data)[3][0];
			AvailRequest.setCode(hotelCode);		
			String checkInDate = sdf1.format(checkIn);		
			AvailRequest.setCheckin(checkInDate);		
			
			Calendar c = Calendar.getInstance(); 
			c.setTime(checkIn); 
			c.add(Calendar.DATE, 1);
			checkOut = c.getTime();			
			
			AvailRequest.setCheckout(sdf1.format(checkOut));
			AvailRequest.getRoomlist().get(0).setBedTypeID(bedType);
		}				
		return AvailRequest;
	}

}
