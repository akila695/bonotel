package com.rezgateway.automation.xmlout.pojo;

public class AvailbilityValidation {
	
	private String ExpectedHotelName = "N/A";
	private String ExpectedCityName ="N/A";
	private String ExpectedRateCurrency = "N/A";
	private String ExpectedRoomCodes = "N/A";
	/**
	 * @return the expectedHotelName
	 */
	public String getExpectedHotelName() {
		return ExpectedHotelName;
	}
	/**
	 * @return the expectedCityName
	 */
	public String getExpectedCityName() {
		return ExpectedCityName;
	}
	/**
	 * @return the expectedRateCurrency
	 */
	public String getExpectedRateCurrency() {
		return ExpectedRateCurrency;
	}
	/**
	 * @return the expectedRoomCodes
	 */
	public String getExpectedRoomCodes() {
		return ExpectedRoomCodes;
	}
	/**
	 * @param expectedHotelName the expectedHotelName to set
	 */
	public void setExpectedHotelName(String expectedHotelName) {
		ExpectedHotelName = expectedHotelName;
	}
	/**
	 * @param expectedCityName the expectedCityName to set
	 */
	public void setExpectedCityName(String expectedCityName) {
		ExpectedCityName = expectedCityName;
	}
	/**
	 * @param expectedRateCurrency the expectedRateCurrency to set
	 */
	public void setExpectedRateCurrency(String expectedRateCurrency) {
		ExpectedRateCurrency = expectedRateCurrency;
	}
	/**
	 * @param expectedRoomCodes the expectedRoomCodes to set
	 */
	public void setExpectedRoomCodes(String expectedRoomCodes) {
		ExpectedRoomCodes = expectedRoomCodes;
	}
	

}
