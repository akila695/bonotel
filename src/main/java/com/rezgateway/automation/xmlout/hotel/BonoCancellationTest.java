package com.rezgateway.automation.xmlout.hotel;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.rezgateway.automation.JavaHttpHandler;
import com.rezgateway.automation.builder.request.AvailabilityRequestBuilder;
import com.rezgateway.automation.builder.request.CancellationRequestBuilder;
import com.rezgateway.automation.builder.request.ReservationRequestBuilder;
import com.rezgateway.automation.input.ExcelReader;
import com.rezgateway.automation.pojo.AvailabilityResponse;
import com.rezgateway.automation.pojo.CancellationRequest;
import com.rezgateway.automation.pojo.Hotel;
import com.rezgateway.automation.pojo.HttpResponse;
import com.rezgateway.automation.pojo.RateplansInfo;
import com.rezgateway.automation.pojo.ReservationRequest;
import com.rezgateway.automation.pojo.ReservationResponse;
import com.rezgateway.automation.pojo.Room;
import com.rezgateway.automation.reader.response.AvailabilityResponseReader;
import com.rezgateway.automation.reader.response.ReservationResponseReader;
import com.rezgateway.automation.reports.ExtentTestNGReportBuilderExt;
import com.rezgateway.automation.xmlout.utill.DataLoader;

public class BonoCancellationTest extends ExtentTestNGReportBuilderExt {

		private static int senarioID = 1;
		private String TestUrlres    = "N/A";
		private String Testurl       = "N/A";
		private String TestUrlcnx    = "N/A";

		@Parameters({ "TestUrlRes", "TestUrl" ,"TestUrlCnx" })
		@Test
		public void setUp(String TestUrlRes, String TestUrl ,String TestUrlCnx){
		   	this.TestUrlres = TestUrlRes;
		   	this.Testurl = TestUrl;
		   	this.TestUrlcnx = TestUrlCnx;
		}
		@Test(dataProvider = "getReservationDataInExcel" ,dependsOnMethods = "setUp")
		public synchronized void cancellationTest(ReservationRequest Request) throws IOException, Exception {

			ITestResult results = Reporter.getCurrentTestResult();
			String testname = "Test number  " + senarioID + " : Reservation of  : " + Request.getCode()[0];
			results.setAttribute("TestName", testname);
			results.setAttribute("Expected", " System should be cancel this hotel succesfully");
			// http://192.168.1.62:8380/bonotelapps/bonotel/reservation/GetReservation.do

			// === Availability Check Started === /

			JavaHttpHandler handler = new JavaHttpHandler();

			HttpResponse Av_Response = handler.sendPOST( Testurl ,"xml=" + new AvailabilityRequestBuilder().buildRequest("Resources/Sample_AvailRequestNew_senarioID_" + senarioID + ".xml", Request));
			senarioID++;
			if (Av_Response.getRESPONSE_CODE() == 200) {
				AvailabilityResponse AvailabilityResponse = new AvailabilityResponseReader().getResponse(Av_Response.getRESPONSE());
				if (AvailabilityResponse.getHotelCount() > 0) {
					results.setAttribute("Actual", "Results Available, Hotel Count :" + AvailabilityResponse.getHotelCount());
					// === Reservation Request Data Initiated === //
					Hotel hotel = AvailabilityResponse.getHotelList().entrySet().iterator().next().getValue();
					Request.setCurrency(hotel.getRateCurrencyCode());

					Iterator<Entry<String, ArrayList<Room>>> itr = hotel.getRoomInfo().entrySet().iterator();
					
					Double TotalRate = 0.00;
					Double TotalTax  = 0.00;	
					int i = 0 ;
					while (itr.hasNext()) {
						Map.Entry<String, ArrayList<Room>> entry = (Map.Entry<String, ArrayList<Room>>) itr.next();
					    //String roomnum = entry.getKey().trim();
						Room   room    = entry.getValue().get(0);
						room.setAdultsCount(Request.getRoomlist().get(i).getAdultsCount());
						room.setChildCount(Request.getRoomlist().get(i).getChildCount());
						room.setChildAges(Request.getRoomlist().get(i).getChildAges());
						Entry<String, RateplansInfo>  plan = room.getRatesPlanInfo().entrySet().iterator().next();
						RateplansInfo planinfo = plan.getValue();
						room.setRatePlanCode(planinfo.getRatePlanCode());
						Double atax = planinfo.getTaxInfor().get("roomTax").getTaxAmount();
						Double btax = planinfo.getTaxInfor().get("salesTax").getTaxAmount();
						Double ctax = planinfo.getTaxInfor().get("otherCharges").getTaxAmount();
						TotalRate +=  planinfo.getTotalRate();
						TotalTax  += (atax+btax+ctax);
						i++;
						Request.addToRezRoomList(room);
					}
					
					Request.setTotal(Double.toString(TotalRate));
					Request.setTotalTax(Double.toString(TotalTax));
					// === Reservation Request Data Initiated === //
				} else {
					results.setAttribute("Actual", "Results not available Error Code :" + AvailabilityResponse.getErrorCode() + " Error Desc :" + AvailabilityResponse.getErrorDescription());
					Assert.fail("No Results Error Code :" + AvailabilityResponse.getErrorCode());
				}

			} else {
				results.setAttribute("Actual", "No Response recieved Code :" + Av_Response.getRESPONSE_CODE());
				Assert.fail("Invalid Response Code :" + Av_Response.getRESPONSE_CODE() + " ,No Response received");
			}

			// === Availability Check End === /

			HttpResponse Response = handler.sendPOST(TestUrlres,"xml=" + new ReservationRequestBuilder().buildRequest("Resources/Sample_ReservationRequest_senarioID_" + senarioID + ".xml", Request));

			if (Response.getRESPONSE_CODE() == 200) {
				ReservationResponse resResponse = new ReservationResponseReader().getResponse(Response.getRESPONSE());
				
				CancellationRequest cnx = new CancellationRequest();
				LocalDateTime date = LocalDateTime.now();
				DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
				
				cnx.setCancellationRequestTimestamp(date.format(formatter));
				cnx.setUserName(Request.getUserName());
				cnx.setPassword(Request.getPassword());
				cnx.setSupplierReferenceNo(resResponse.getReferenceno());
				cnx.setCancellationNotes("Cancellation_note_senarioID__" + senarioID);
				cnx.setCancellationReason("Cancellation_Reason_senarioID__" + senarioID);
				
				HttpResponse cnxResponse = handler.sendPOST(TestUrlcnx ,"xml=" + new CancellationRequestBuilder().buildRequest("Resources/Sample_CancellationRequest_senarioID_" + senarioID + ".xml", cnx));

				if(cnxResponse.getRESPONSE_CODE()== 200){
					results.setAttribute("Actual", "Reservation Done: " +resResponse.getReferenceno() + " / " + cnx.getSupplierReferenceNo());
				}else{
					results.setAttribute("Actual", "No Response recieved Code :" + cnxResponse.getRESPONSE_CODE());
					Assert.fail("Invalid Response Code :" + cnxResponse.getRESPONSE_CODE() + " ,No Response received");
				}
				
			} else {
				results.setAttribute("Actual", "No Response recieved Code :" + Response.getRESPONSE_CODE());
				Assert.fail("Invalid Response Code :" + Response.getRESPONSE_CODE() + " ,No Response received");
			}

		}

		
		@DataProvider(name = "getReservationDataInExcel")
		public ReservationRequest[][] getReservationDataInExcel() throws Exception {

			ExcelReader reader = new ExcelReader();
			DataLoader loader = new DataLoader();
			return loader.getReservationReqObjList(reader.getExcelData("Resources/HotelScenarios.xls", "CancellationReq"));
		}
}

	

